# -*- coding: utf-8 -*-

import os
import platform
import smtplib
import getpass
#import mimetypes

from email.utils import formataddr
from email.utils import formatdate
from email.utils import COMMASPACE

from email.header import Header
from email import encoders

from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.mime.image import MIMEImage

import time, datetime

def tip(t,n):
    plat = platform.system()
    if plat == "Windows":
        print(t)
        v = os.system("ping "+ t + " -n " + str(n))
        return v
    elif plat == "Linux":
        v = os.system("ping -c "+ str(n) + " " + t)
        return v

def vpn_rasdial(name_vpn_lan, login_vpn, password_vpn):
    plat = platform.system()
    if plat == "Windows":
        vp = os.system('rasdial '+ name_vpn_lan + " " + login_vpn + " " + password_vpn)
        return vp
    elif plat == "Linux":
        pass

def Email_to_log(recipient_addr, name_ip, status_ip, ip):
    sender_name = 'MEDABOUT.ME'# Наименование Организации
    sender_addr = 'a.konovalov@medabout.me'# Email  от кого
    passwd_d = 'bkw35528654wkb'# Пароль
    smtp = 'smtp.gmail.com' # server smtp
    port = '587' # port
    #recipient_addr = [] # список адресов
    subject = 'MEDABOUT.ME VPN Info'# тема письма

    text = """
Это сообщение было отправленно автоматически VPN шлюзом.
   
Date time: """ + str(datetime.datetime.now()) + """
Login: """ + name_ip + """
Ваш IP: """  + ip + """
Статус подключения VPN: """ + status_ip + """
   
По вопросам обращайтесь к администратору системы
Email: a.konovalov@medabout.me"""

    #passwd = getpass.getpass('Password: ')
    passwd = passwd_d

    sender_name = Header(sender_name, 'utf-8').encode()

    msg_root = MIMEMultipart('mixed')
    msg_root['Date'] = formatdate(localtime=1)
    msg_root['From'] = formataddr((sender_name, sender_addr))
    msg_root['To'] = COMMASPACE.join(recipient_addr)
    msg_root['Subject'] = Header(subject, 'utf-8')
    msg_root.preamble = 'This is a multi-part message in MIME format.'

    msg_related = MIMEMultipart('related')
    msg_root.attach(msg_related)

    msg_alternative = MIMEMultipart('alternative')
    msg_related.attach(msg_alternative)

    msg_text = MIMEText(text.encode('utf-8'), 'plain', 'utf-8')
    msg_alternative.attach(msg_text)
    
    mail_server = smtplib.SMTP(smtp, port)
    mail_server.ehlo()

    try:
        mail_server.starttls()
        mail_server.ehlo()
    except smtplib.SMTPException as e:
        print(e)

    mail_server.login(sender_addr, passwd)
    mail_server.send_message(msg_root)
    mail_server.quit()
    
def Email_to_file_log(recipient_addr, name_file):
    sender_name = 'MEDABOUT.ME'# Наименование Организации
    sender_addr = 'a.konovalov@medabout.me'# Email  от кого
    passwd_d = 'bkw35528654wkb'# Пароль
    smtp = 'smtp.gmail.com' # server smtp
    port = '587' # port
    #recipient_addr = [] # список адресов
    subject = 'MEDABOUT.ME VPN Info'# тема письма
    attachments = name_file
    text = """Файл LOG на Date time: """ + str(datetime.datetime.now())

    #passwd = getpass.getpass('Password: ')
    passwd = passwd_d

    sender_name = Header(sender_name, 'utf-8').encode()

    msg_root = MIMEMultipart('mixed')
    msg_root['Date'] = formatdate(localtime=1)
    msg_root['From'] = formataddr((sender_name, sender_addr))
    msg_root['To'] = COMMASPACE.join(recipient_addr)
    msg_root['Subject'] = Header(subject, 'utf-8')
    msg_root.preamble = 'This is a multi-part message in MIME format.'

    msg_related = MIMEMultipart('related')
    msg_root.attach(msg_related)

    msg_alternative = MIMEMultipart('alternative')
    msg_related.attach(msg_alternative)

    msg_text = MIMEText(text.encode('utf-8'), 'plain', 'utf-8')
    msg_alternative.attach(msg_text)
    
    for attachment in attachments:
        fname = os.path.basename(attachment)

        with open(attachment, 'rb') as f:
            msg_attach = MIMEBase('application', 'octet-stream')
            msg_attach.set_payload(f.read())
            encoders.encode_base64(msg_attach)
            msg_attach.add_header('Content-Disposition', 'attachment',
                                  filename=(Header(fname, 'utf-8').encode()))
            msg_root.attach(msg_attach)

    mail_server = smtplib.SMTP(smtp, port)
    mail_server.ehlo()

    try:
        mail_server.starttls()
        mail_server.ehlo()
    except smtplib.SMTPException as e:
        print(e)

    mail_server.login(sender_addr, passwd)
    mail_server.send_message(msg_root)
    mail_server.quit()

