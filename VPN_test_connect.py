# -*- coding: utf-8 -*-
import threading
import sistem_p
import time
import datetime
import os

sp_error = []
sp_ok = []

sp_log_VPN = []
sp_IP_prinadl = []
sp_ip_all = []

n_sec = 120

name_vpn_lan = "test_vpn"
login_vpn = "server-test"
password_vpn = "test1234"

server_vpn = "vpn.labgate.ru"
vpn_connect = "10.0.0.33"

file_statistics = "statistics.txt"
ip_file = "parametr.txt"


status_off = "off"
status_on = "online"
restart_vpn = "Restart VPN_Test"
email_sp_dop = ["a.konovalov@medabout.me", "i.rozhkov@medabout.me"]

def Email_to(ip,status_ip):
    global sp_ip_all, email_sp_dop

    e = 0
    i = 0
    while i < len(sp_ip_all):
        if ip == sp_ip_all[i][1]:
            email_temp_sp = []
            email_temp_sp = email_sp_dop + sp_ip_all[i][2]
            print("Отправленнно !!!!!!!!!!!", status_ip, ip, email_temp_sp)
            sistem_p.Email_to_log(email_temp_sp, sp_ip_all[i][0],status_ip, ip)
            e = 1
        else:
            pass
        i+=1
    if e == 0:
        print("Отправил себе ИНФО!!!!!!!!!!!!!!!!!!!", status_ip, ip, email_sp_dop)
        sistem_p.Email_to_log(email_sp_dop, ip, status_ip, ip)
    else:
        pass

def record_file_txt(namme_status, ip_connect):
    myfile = open(file_statistics, "a")

    now_date = datetime.date.today()# Текущая дата (без времени)
    now_time = datetime.datetime.now()# Текущая дата со временем

    cur_hour = now_time.hour # Час текущий
    cur_minute = now_time.minute# Минута текущая
    cur_second = now_time.second# Секунда текущие

    text_txt = ""
    text_txt = str(now_date)+ "\t"
    text_txt += str(cur_hour) + ":" + str(cur_minute) + ":" + str(cur_second) + "\t"
    text_txt += namme_status + "\t"
    text_txt += ip_connect + "\n"

    myfile.write(text_txt)
    myfile.close()

    print(text_txt)
    #print(n_stoka, now_time, namme_status, ip_connect)


def logi_VPN(namme_status, ip_connect, signal):
    global sp_log_VPN, sp_IP_prinadl, status_off, status_on

    if signal == "0": # проверка сиглнала сообщения
        s = 0
    elif signal == "1":
        s = 1

    if sp_log_VPN == [] and sp_IP_prinadl == []: # если список IP пучстой, добавляем IP, LOG-сообщение, сигнал сообщения
        tmp = []
        tmp.append(namme_status)
        tmp.append(ip_connect)
        tmp.append(s)
        sp_log_VPN.append(tmp)
        sp_IP_prinadl.append(ip_connect)
        record_file_txt(namme_status, ip_connect)


    elif ip_connect in sp_IP_prinadl:
        i = 0
        while i < len(sp_IP_prinadl):
            if sp_IP_prinadl[i] == ip_connect:
                if namme_status == sp_log_VPN[i][0] and s == 0:
                    #print ("logi_VPN -> namme_status == sp_log_VPN[i][0] and s == 0:")
                    pass
                elif namme_status == sp_log_VPN[i][0] and s == 1:
                    if sp_log_VPN[i][2] == 2:
                        sp_log_VPN[i][2] += 1
                        record_file_txt("Email to status " + status_off, ip_connect)
                        Email_to(ip_connect, status_off)# Отправляем Емаил
                    elif sp_log_VPN[i][2] < 3:
                        sp_log_VPN[i][2] += 1
                elif namme_status != sp_log_VPN[i][0] and s == 0:
                    if sp_log_VPN[i][2] > 2:
                        record_file_txt("Email to status " + status_on, ip_connect)
                        Email_to(ip_connect, status_on)# Отправляем Емаил
                    record_file_txt(namme_status, ip_connect)
                    sp_log_VPN[i][0] = namme_status
                    sp_log_VPN[i][2] = 0
                elif namme_status != sp_log_VPN[i][0] and s == 1:
                    sp_log_VPN[i][0] = namme_status
                    record_file_txt(namme_status, ip_connect)
                    if sp_log_VPN[i][2] == 2:
                        sp_log_VPN[i][2] += 1
                        record_file_txt("Email to status " + status_off, ip_connect)
                        Email_to(ip_connect, status_off)# Отправляем Емаил
                    elif sp_log_VPN[i][2] < 3:
                        sp_log_VPN[i][2] += 1
            else:
                pass
            i+=1

    else:
        tmp = []
        tmp.append(namme_status)
        tmp.append(ip_connect)
        tmp.append(s)
        sp_log_VPN.append(tmp)
        sp_IP_prinadl.append(ip_connect)
        record_file_txt(namme_status, ip_connect)

    print(sp_log_VPN)


def pausa_ping():
    global sp_error, sp_ok, status_off, status_on, n_sec

    if sp_error != []:
        #print(sp_error)
        for sp_er in sp_error:
            logi_VPN(status_off, sp_er, "1")
        sp_error = []
    else:
        pass
    if sp_ok != []:
        #print(sp_ok)
        for oki in sp_ok:
            logi_VPN(status_on, oki, "0")
        sp_ok = []
    else:
        pass
    print("Пауза "+ str(n_sec) + " сек")
    now_time = datetime.datetime.now()
    print(now_time)
    time.sleep(int(n_sec))




server_stop = True
ping_server = True

class Connect(threading.Thread):
    def __init__(self, ip):
        self.ip = ip
        threading.Thread.__init__(self)
    def run (self):
        global server_stop, sp_error, sp_ok
        while server_stop:
            halt = sistem_p.tip(self.ip,2)
            if halt == 0:
                print("OK ", self.ip)
                sp_ok.append(self.ip)
                server_stop = False
                break
            else:
                print("OFF ", self.ip)
                sp_error.append(self.ip)
                server_stop = False
                return sp_error
                break
                

def ip_spisok():
    global sp_ip_all
    sp_ip_all = []
    sp_ip = []
    file = open(ip_file, "r")
    lines = file.readlines()
    for line in lines:
        k = line.split('|')
        sp_ip.append(k)
    file.close()
    #print(sp_ip)

    for sp in sp_ip:
        if sp == ['\n']:
            pass
        else:
            i = 0
            tmp = []
            tmp1 = []
            for p in sp:
                if i == 0 or i == 1:
                    tmp.append(p)
                elif i > 1:
                    tmp1.append(p[:-1])
                i+=1
            tmp.append(tmp1)
            sp_ip_all.append(tmp)

    return sp_ip_all

def connect():
    global status_on, status_off
    vpn_wat = 0
    dostup_vpn = sistem_p.tip(server_vpn, 1)
    if dostup_vpn == 0:
        connect_vpn = sistem_p.tip(vpn_connect, 1)
        if connect_vpn == 0:
            pass
        else:
            connect_vpn_utoch = sistem_p.tip(vpn_connect, 4)
            if connect_vpn_utoch == 0:
                print("Уточняющий OK ")
                pass
            else:
                logi_VPN(status_off, vpn_connect, "1")
                vp = sistem_p.vpn_rasdial(name_vpn_lan, login_vpn, password_vpn)
                if vp == 0:
                    logi_VPN(status_on, vpn_connect, "0")
                    print("Connect VPN  OK !!!")
                else:
                    vpn_wat = 1
    else:
        dostup_vpn = sistem_p.tip(server_vpn, 4)
        if dostup_vpn == 0:
            print(server_vpn, status_on)
            logi_VPN(status_on, server_vpn, "0")
        else:
            print(server_vpn, status_off)
            logi_VPN(status_off, server_vpn, "1")
            vpn_wat = 1
    return vpn_wat

def start_server_ping():
    #global sp_ip_all
    sp_ip_all = ip_spisok()
    i = 0
    n_sp = len(sp_ip_all)
    while i < n_sp:
        Connect(sp_ip_all[i][1]).start()
        #print(ip_sp[i])
        i+=1

#ip_spisok()
logi_VPN(restart_vpn, server_vpn, "0")

while ping_server:
    vpn_test = connect()
    if vpn_test == 0:
        start_server_ping()
    else:
        pass
    server_stop = True
    pausa_ping()


