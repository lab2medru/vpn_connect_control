# -*- coding: utf-8 -*-
import sistem_p
import time
import datetime


sp_ip_all = []
ip_file = "parametr.txt"
file = "statistics.txt"
email_sp_dop = ["a.konovalov@medabout.me"]
n_sec = "1"

def record_file_txt(namme_status, ip_connect):
    myfile = open(file, "a")

    now_date = datetime.date.today()# Текущая дата (без времени)
    now_time = datetime.datetime.now()# Текущая дата со временем

    cur_hour = now_time.hour # Час текущий
    cur_minute = now_time.minute# Минута текущая
    cur_second = now_time.second# Секунда текущие

    text_txt = ""
    text_txt = str(now_date)+ "\t"
    text_txt += str(cur_hour) + ":" + str(cur_minute) + ":" + str(cur_second) + "\t"
    text_txt += namme_status + "\t"
    text_txt += str(ip_connect) + "\n"

    myfile.write(text_txt)
    myfile.close()

    print(text_txt)

def file_log_to():
    global file, email_sp_dop
    file_log = []
    file_log.append(file)
    sistem_p.Email_to_file_log(email_sp_dop,file_log)
    record_file_txt("Email to Log file", "Email admin")
    print("Email")

def pause_time(n_sec):
    time.sleep(int(n_sec))

server_time = True
time_to_hour = 7
time_to_minute = 30
print("Email to file: ",str(time_to_hour) + ":" + str(time_to_minute))

while server_time:
    now_time = datetime.datetime.now()# Текущая дата со временем
    cur_hour = now_time.hour # Час текущий
    cur_minute = now_time.minute# Минута текущая
    cur_second = now_time.second# Секунда текущие
    if cur_hour == time_to_hour and cur_minute == time_to_minute and cur_second == 0:
        file_log_to()
    #print(cur_hour, cur_minute, cur_second)
    pause_time(n_sec)

